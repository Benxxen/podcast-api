package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog/log"
	"net/http"
	"time"
)

type podcastFetcherConfig struct {

}

func main() {
	db, err := createDatabaseConnection()
	if err != nil {
		log.Error().Err(err).Msg("couldn´t create database connection")
	}

	runServer(db)
}

func runServer(db *sqlx.DB)  {
	r := mux.NewRouter()

	r.HandleFunc("/alive", statusAlive).Methods(http.MethodGet)
	r.HandleFunc("/update", fetchTopPodcasts(db)).Methods(http.MethodGet)
	r.HandleFunc("/rank/{id}", readPodcastRank(db)).Methods(http.MethodGet)

	server := http.Server{
		Addr:              ":9000",
		Handler:           r,
		ReadTimeout:       time.Duration(15) * time.Second,
		WriteTimeout:      time.Duration(15) * time.Second,
		IdleTimeout:       time.Duration(60) * time.Second,
	}

	fmt.Println("start server")
	err := server.ListenAndServe()
	if err != nil {
		log.Error().Err(err).Msg("couldn´t start server")
	}
}