package main

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func createDatabaseConnection() (*sqlx.DB,error){
	db, err := sqlx.Connect("postgres", "user=test_user password=test dbname=podcasts sslmode=disable")
	if err != nil {
		return nil, fmt.Errorf("couldn´t connect to database: %w", err)
	}

	return db, nil
}


func insertPodcastRank(db *sqlx.DB, podRank []podcastRank) error {
	for _, rank := range podRank {
		_, err := db.NamedExec("INSERT INTO podcasts.public.podcastrank(podcastid, countrycode, title, rank) VALUES (:PodcastID, :CountryCode, :Title, :Rank)",
			map[string]interface{}{
				"PodcastID": rank.PodcastID,
				"CountryCode": rank.CountryCode,
				"Title": rank.Title,
				"Rank": rank.Rank,
			},
		)
		if err != nil {
			return fmt.Errorf("couldn´t insert podcastRank: %w", err)
		}
	}
	return nil
}

func readPodcastRanking(db *sqlx.DB, podcastId string) ([]map[string]int, error) {
	podcastRanking := []map[string]int{}

	result, err := db.NamedQuery("SELECT countrycode, rank FROM podcasts.public.podcastrank WHERE podcastid = :id", map[string]interface{}{"id": podcastId})

	if err != nil {
		return podcastRanking, fmt.Errorf("couldn´t read podcast ranking: %w", err)
	}

	defer result.Close()
	for result.Next() {
		var podRank podcastRank
		err = result.StructScan(&podRank)
		if err != nil {
			return podcastRanking, fmt.Errorf("couldn´t scan db result: %w, got: %v", err, result)
		}
		ranking := map[string]int{podRank.CountryCode: podRank.Rank}
		podcastRanking = append(podcastRanking, ranking)
	}

	return podcastRanking, nil
}

func updatePodcastRank(db *sqlx.DB, podRank []podcastRank) error {
	for _, rank := range podRank {
		_, err := db.NamedExec("UPDATE podcasts.public.podcastrank SET podcastid=:PodcastID, title=:Title WHERE rank=:Rank AND countrycode=:CountryCode ",
			map[string]interface{}{
				"PodcastID": rank.PodcastID,
				"CountryCode": rank.CountryCode,
				"Title": rank.Title,
				"Rank": rank.Rank,
			},
		)
		if err != nil {
			return fmt.Errorf("couldn´t insert podcastRank: %w", err)
		}
	}
	return nil
}

func checkIfRankingExists(db *sqlx.DB, countryCode string) (bool, error) {
	var count int

	row := db.QueryRowx("SELECT count(*) FROM podcasts.public.podcastrank WHERE countrycode= $1", countryCode)
	err := row.Scan(&count)
	if err != nil {
		return false, fmt.Errorf("couldn´t fetch podcast ranking: %w", err)
	}

	if count == 0{
		return false, err
	}

	return true, nil
}