package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
	"github.com/rs/zerolog/log"
	"io/ioutil"

	//"github.com/rs/zerolog/log"
	"net/http"
	"time"
)

type podcastFeed struct {
	Feed values `json:"feed"`
}

type values struct {
	Podcasts []podcast `json:"entry"`
}

type podcast struct {
	ID map[string]map[string]string `json:"id"`
	Label label `json:"im:name"`
	Artist label `json:"im:artist"`
	Category map[string]map[string]string `json:"category"`
}

type label struct {
	Label string `json:"label"`
}


type podcastRank struct {
	PodcastID string `db:"podcastid"`
	CountryCode string `db:"countrycode"`
	Title string `db:"title"`
	Rank int `db:"rank"`
}


func statusAlive(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	_,_ = w.Write([]byte("\"Ok\""))
}

func fetchTopPodcasts(db *sqlx.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		countryCodes := []string{"us", "de", "fr", "it", "es"}

		client := http.Client{Timeout: time.Duration(5 * time.Second)}

		for _, countryCode := range countryCodes {
			request, err := http.NewRequest("GET", fmt.Sprintf("https://itunes.apple.com/%s/rss/topaudiopodcasts/limit=100/json", countryCode), nil)
			if err != nil {
				http.Error(w, fmt.Sprintf("couldn´t create request: %s", err.Error()), http.StatusInternalServerError)
				log.Error().Err(err).Msg("couldn´t create request")

				return
			}

			resp, err := client.Do(request)
			if err != nil {
				http.Error(w, fmt.Sprintf("couldn´t perform request: %s", err.Error()), http.StatusInternalServerError)
				log.Error().Err(err).Msg("couldn´t perform request")

				return
			}

			defer resp.Body.Close()

			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				http.Error(w, fmt.Sprintf("couldn´t read response body: %s", err.Error()), http.StatusInternalServerError)
				log.Error().Err(err).Msg("couldn´t read response body")

				return
			}

			var feed podcastFeed
			err = json.Unmarshal(body, &feed)
			if err != nil {
				http.Error(w, fmt.Sprintf("couldn´t decode response body: %s", err.Error()), http.StatusInternalServerError)
				log.Error().Err(err).Msg("couldn´t decode response body")

			}


			podRank := convertToPodcastRank(feed, countryCode)
			exists, err := checkIfRankingExists(db, countryCode)

			if err != nil {
				fmt.Println(err)
			}

			if exists != true {
				err = insertPodcastRank(db, podRank)
				if err != nil {
					http.Error(w, fmt.Sprintf("couldn´t insert podcast rank: %s", err.Error()), http.StatusInternalServerError)
					log.Error().Err(err).Msg("couldn´t insert podcast rank")

					return
				}
			}

			err = updatePodcastRank(db, podRank)
			if err != nil {
				http.Error(w, fmt.Sprintf("couldn´t update podcast rank: %s", err.Error()), http.StatusInternalServerError)
				log.Error().Err(err).Msg("couldn´t update podcast rank")

				return
			}

			log.Info().Msg(fmt.Sprintf("successfully updated country: %s", countryCode))

		}


		_,_ = w.Write([]byte("\"Ok\""))

		log.Info().Msg("updated top podcasts successfully")

	}
}
func readPodcastRank(db *sqlx.DB) http.HandlerFunc{
	return func(w http.ResponseWriter, r *http.Request) {
		podcastId := mux.Vars(r)["id"]

		result, err := readPodcastRanking(db, podcastId)
		if err != nil {
			http.Error(w, fmt.Sprintf("%s", err.Error()), http.StatusInternalServerError)
			log.Error().Err(err).Msg("couldn´t read podcast ranking")

			return
		}

		jsonResponse, err := json.Marshal(result)
		w.WriteHeader(http.StatusOK)
		_,_ = w.Write(jsonResponse)
	}
}

func convertToPodcastRank(feed podcastFeed, countryCode string) []podcastRank  {
	var ranking []podcastRank

	for rank, podcast := range feed.Feed.Podcasts {
		var podRank podcastRank
		podRank.Rank = rank + 1
		podRank.PodcastID = podcast.ID["attributes"]["im:id"]
		podRank.Title = podcast.Label.Label
		podRank.CountryCode = countryCode

		ranking = append(ranking, podRank)
	}

	return ranking
}
