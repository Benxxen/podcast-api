CREATE TABLE PodcastRank
(
    PodcastID    CHAR(15) NOT NULL,
    CountryCode  CHAR(2) NOT NULL,
    Title        CHAR(100) NOT NULL,
    Rank         INT,
    PRIMARY KEY (PodcastID, CountryCode)
);