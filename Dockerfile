FROM golang:alpine

RUN mkdir /app
ADD . /app
WORKDIR /app
RUN go build -o main --mod=vendor ./cmd/api
CMD ["/app/main"]