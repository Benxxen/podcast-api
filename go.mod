module podcast-api

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.3
	github.com/lib/pq v1.2.0
	github.com/rs/zerolog v1.21.0
)
