## Podcast -API

### Description
This application should allow the user to fetch the top 100 podcasts from itunes for different countries and store them inside an Postgres database.

### Development Setup
#### Actual Setup:

Postgres Port: 5432;
Postgres DB: podcasts

#### Run Docker
```shell
docker-compose up
```

#### Build Application
Inside docker:
```shell
docker build -t podcast-api .
```

OR

```shell
go build -o . --mod=vendor ./cmd/api
```

#### Run Application
Inside docker:
```shell
docker run podcast-api
```

OR

```shell
./api
```


